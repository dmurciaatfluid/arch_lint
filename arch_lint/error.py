class BrokenArch(Exception):
    """Error when an architecture violation occurs."""
